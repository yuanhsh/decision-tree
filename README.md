# README #

Data Mining for CRM

### Description ###

This is a java implementation of ID3 decision tree algorithm.

### Usage ###

java ID3 data/trainProdSelection.arff data/testProdSelection.arff

### Reference ###

[Learning Decision Trees](http://www.cedar.buffalo.edu/~srihari/CSE574/Chap16/Chap16.2-DataSets.pdf)