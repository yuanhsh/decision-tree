
public class Instance {

	private InstanceSet dataset; // the data set this instance belongs to
	private double[] attrValues; // the array of values, stores index for symbolic attributes
	
	public Instance(InstanceSet dataset, double[] attrValues) {
		this.dataset = dataset;
		this.attrValues = attrValues;
	}
	
	public Instance(InstanceSet dataset, Instance inst) {
		this.dataset = dataset;
		this.attrValues = inst.attrValues;
	}
	
	public InstanceSet getDataset() {
		return dataset;
	}
	public double[] getAttrValues() {
		return attrValues;
	}
	public void setDataset(InstanceSet dataset) {
		this.dataset = dataset;
	}
	public void setAttrValues(double[] attrValues) {
		this.attrValues = attrValues;
	}
	
	public Attribute attribute(int index) {
		return dataset.getAttributes().get(index);
	}
	
	public double valueOf(Attribute attribute) {
		return this.attrValues[attribute.getIndex()];
	}
	
	public double valueAt(int i) {
		return attrValues[i];
	}
	
	public String strValueAt(int i) {
		if(i < 0) return "";
		StringBuilder sb = new StringBuilder();
		if(attribute(i).isNumeric()) {
			sb.append(attrValues[i]);
		} else {
			sb.append(attribute(i).valueAt((int)attrValues[i]));
		}
		return sb.toString();
	}
	
	/**
	 * get the value of class/label
	 * @return the index of class value
	 */
	public int classValue() {
		return (int)valueOf(dataset.classAttribute());
	}
	
	public String classStrValue() {
		return strValueAt(dataset.classAttribute().getIndex());
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<attrValues.length; i++) {
			if(sb.length() > 0) sb.append(",");
			sb.append(strValueAt(i));
		}
		return sb.toString();
	}
}
