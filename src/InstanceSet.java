import java.io.*;
import java.util.*;

public class InstanceSet {

	private String relation;
	private List<Attribute> attributes = new ArrayList<>();
	private List<Instance> instances = new ArrayList<>();
	
	public InstanceSet(Reader reader) throws IOException {
		BufferedReader br = new BufferedReader(reader);
		try {
			readARFF(br);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
	}
	
	public InstanceSet(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			readARFF(br);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
	}
	
	public InstanceSet(InstanceSet data) {
		this.relation = data.relation;
		this.attributes = data.attributes;
	}
	
	public InstanceSet(InstanceSet data, boolean copyInstances) {
		this(data);
		if(copyInstances) {
			data.copyTo(this, 0, data.instanceSize());
		}
	}
	
	/**
	 * Construct InstanceSet by reading the ARFF file
	 * @param reader the reader of ARFF file
	 * @throws IOException
	 */
	private void readARFF(BufferedReader reader) throws IOException {
		String line;
		while((line = reader.readLine()) != null) {
			if(line.isEmpty()) continue;
			if(line.startsWith("@relation")) {
				this.relation = line.substring("@relation ".length());
			} else if(line.startsWith("@attribute")) {
				String[] attr = line.substring("@attribute ".length()).split("\\s");
				if(attr[1].equals("real")) {
					attributes.add(new Attribute(attr[0], attributes.size()));
				} else {
					String[] vals = attr[1].replace("{", "").replace("}", "").split(",");
					List<String> values = Arrays.asList(vals);
					attributes.add(new Attribute(attr[0], values, attributes.size()));
				}
			} else if(line.startsWith("@data")) {
				getInstances(reader);
			}
		}
	}
	
	/**
	 * Read instances from the ARFF file
	 * @param reader the reader of ARFF file
	 * @throws IOException
	 */
	private void getInstances(BufferedReader reader) throws IOException {
		String line;
		while((line = reader.readLine()) != null) {
			if(line.isEmpty()) continue;
			String[] instanceValues = line.split(",");
			double[] attrValues = new double[instanceValues.length];
			for(int i=0; i<instanceValues.length; i++) {
				Attribute attr = attribute(i);
				if(attr.isNumeric()) {
					attrValues[i] = Double.parseDouble(instanceValues[i]);
				} else if(attr.isNominal()) {
					attrValues[i] = attr.indexOf(instanceValues[i]);
				}
			}
			Instance instance = new Instance(this, attrValues);
			this.instances.add(instance);
		}
	}
	
	/**
	 * Sort the instances by attribute
	 * @param attr the attribute
	 */
	public void sortByAttribute(Attribute attr) {
		Collections.sort(this.instances, new Comparator<Instance>() {
			@Override
			public int compare(Instance o1, Instance o2) {
				double diff = o1.valueOf(attr) - o2.valueOf(attr);
				if(diff < -ID3.EPS) return -1;
				else if(diff > ID3.EPS) return 1;
				else return 0;
			}
		});
	}
	
	/**
	 * Add a new instance to the current instance list
	 * @param ins the instance to be added
	 */
	public void addInstance(Instance ins) {
		Instance inst = new Instance(this, ins);
		this.instances.add(inst);
	}
	
	/**
	 * Get the i-th attribute
	 * @param i the index of attribute
	 * @return the attribute
	 */
	public Attribute attribute(int i) {
		return this.attributes.get(i);
	}
	
	/**
	 * Get train data set from current data set
	 * @param numFolds the number of folds
	 * @param numFold the index of folds
	 * @return train data set
	 */
	public InstanceSet crossvalTrainSet(int numFolds, int numFold) {
		int offset = 0;
		int numInstForFold = instanceSize() / numFolds;
		if (numFold < instanceSize() % numFolds) {
			numInstForFold++;
			offset = numFold;
		} else {
			offset = instanceSize() % numFolds;
		}
		InstanceSet train = new InstanceSet(this);
		int first = numFold * (instanceSize() / numFolds) + offset;
		copyTo(train, 0, first);
		copyTo(train, first + numInstForFold, instanceSize() - first - numInstForFold);
		return train;
	}
	
	/**
	 * Get test data set from current data set
	 * @param numFolds the number of folds
	 * @param numFold the index of folds
	 * @return test data set
	 */
	public InstanceSet crossvalTestSet(int numFolds, int numFold) {
		int offset = 0;
		int numInstForFold = instanceSize() / numFolds;
		if (numFold < instanceSize() % numFolds) {
			numInstForFold++;
			offset = numFold;
		} else {
			offset = instanceSize() % numFolds;
		}
		InstanceSet test = new InstanceSet(this);
		int startIndex = numFold * (instanceSize() / numFolds) + offset;
		copyTo(test, startIndex, numInstForFold);
		return test;
	}
	
	/**
	 * Stratify the instances when cross validation, to make sure
	 * the instances with different class label are distributed evenly
	 * @param numFolds the number of folds
	 */
	public void stratify(int numFolds) {
		sortByAttribute(classAttribute());
		int start = 0, j = 0;
		List<Instance> list = new ArrayList<>();
		while (list.size() < instanceSize()) {
			j = start;
			while (j < instanceSize()) {
				list.add(instances.get(j));
				j = j + numFolds;
			}
			start++;
		}
		instances = list;
	}
	
	/**
	 * Copy the instances to the destination InstanceSet
	 * @param dest the destination
	 * @param start start position
	 * @param size size of instances to copy
	 */
	private void copyTo(InstanceSet dest, int start, int size) {
		for (int i = 0; i < size; i++) {
			dest.addInstance(instances.get(start + i));
		}
	}
	
	/**
	 * get the the size of class for the predicted label
	 * @return the size of label/class
	 */
	public int classSize() {
		if(classAttribute().isNominal()) { // for discrete attributes
			return classAttribute().valueSize();
		}
		return 1; // for continuous attributes
	}
	
	/**
	 * get the attribute of the predicted label, here is the last attribute
	 * @return the label attribute
	 */
	public Attribute classAttribute() {
		return attribute(attributes.size()-1);
	}

	public String getRelation() {
		return relation;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public List<Instance> getInstances() {
		return instances;
	}
	
	public int instanceSize() {
		if(instances == null) return 0;
		return instances.size();
	}
	
	public int attributeSize() {
		if(attributes == null) return 0;
		return attributes.size();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("@relation " + this.relation);
		sb.append("\n\n");
		for(Attribute attr: this.attributes) {
			sb.append(attr.toString()).append("\n");
		}
		sb.append("\n@data\n");
		for(Instance ins:this.instances) {
			sb.append(ins.toString()).append("\n");
		}
		return sb.toString();
	}

	public static void main(String[] args) throws Exception {
		Reader reader = new FileReader(args[0]);
		InstanceSet instances = new InstanceSet(reader);
		System.out.println(instances);
	}

}
