
public class ID3 {
	public static double EPS = 1e-6;
	public static double LOG2 = Math.log(2);
	public static boolean DEBUG = false;
	
	private ID3[] children; // decision tree child nodes
	private Attribute attribute; // attribute to split on
	private int classValue; // the index of classification result
	private Attribute classAttribute; // the attribute of class label
	private double[] distribution; // distribution table for class label
	private double threshold; // the split threshold if splitting on continuous attribute
	
	/**
	 * Build the decision tree using ID3 algorithm
	 * @param dataset the data set used to build the tree
	 * @throws Exception
	 */
	public void buildTree(InstanceSet dataset) throws Exception {
		if(dataset.instanceSize() == 0) {
			attribute = null;
			classAttribute = null;
			distribution = new double[dataset.classSize()];
			classValue = -1;
			if(DEBUG) System.out.println("dataset.instanceSize() == 0");
			return;
		}
		double[] gains = new double[dataset.attributeSize()];
		for(Attribute attr:dataset.getAttributes()) {
			if(attr.getIndex() == dataset.classAttribute().getIndex()) continue;
			gains[attr.getIndex()] = computeInfoGain(dataset, attr);
		}
		int maxIndex = maxIndex(gains);
		attribute = dataset.attribute(maxIndex);
		if(attribute.isNumeric()) {
			threshold = attribute.getThreshold();
		}
		
		if(eq(gains[maxIndex], 0) || dataset.instanceSize() <= 5) { // Online-Pruning: minNumObjects=5
			attribute = null;
			distribution = new double[dataset.classSize()];
			for(Instance inst:dataset.getInstances()) {
				distribution[inst.classValue()]++;
			}
			normalize(distribution);
			classValue = maxIndex(distribution);
			classAttribute = dataset.classAttribute();
		} else {
			InstanceSet[] splitData = splitData(dataset, attribute);
			children = new ID3[splitData.length];
			for (int j = 0; j < splitData.length; j++) {
				children[j] = new ID3();
				children[j].buildTree(splitData[j]);
			}
		}
	}
	
	/**
	 * Classifies a given test instance using the decision tree.
	 * @param instance the instance to be classified
	 * @return the classification
	 */
	public int classifyInstance(Instance instance) {
		int index = 0;
		if (attribute == null) {
			return classValue;
		} else if (attribute.isNominal()) {
			index = (int)instance.valueOf(attribute);
		} else if (attribute.isNumeric()) {
			index = instance.valueOf(attribute) <= this.threshold ? 0 : 1;
		}
		return children[index].classifyInstance(instance);
	}
	
	/**
	 * Classifies a given test instance using the decision tree.
	 * @param instance the instance to be classified
	 * @return the string label of class
	 */
	public String stringClassify(Instance instance) {
		int idx = (int)classifyInstance(instance);
		return classAttribute.valueAt(idx);
	}
	
	/**
	 * Compute the information gain of the the specified attribute
	 * @param data the data set used to compute info gain
	 * @param attribute the attribute used to compute info gain
	 * @return information gain
	 */
	private double computeInfoGain(InstanceSet data, Attribute attribute) {
		if(attribute.isNumeric()) {
			return computeNumericInfoGain(data, attribute);
		}
		double infoGain = computeEntropy(data);
		InstanceSet[] splitData = splitData(data, attribute);
		for (int i = 0; i < splitData.length; i++) {
			if (splitData[i].instanceSize() > 0) {
				infoGain -= ((double) splitData[i].instanceSize() / 
						(double) data.instanceSize()) * computeEntropy(splitData[i]);
			}
		}
		return infoGain;
	}
	
	/**
	 * Compute the information gain of the the numeric attribute
	 * @param data the data set used to compute info gain
	 * @param attribute the attribute used to compute info gain
	 * @return information gain
	 */
	private double computeNumericInfoGain(InstanceSet data, Attribute attribute) {
		double maxInfoGain = 0, threshold = -1;
		double infoGain = computeEntropy(data);
		data.sortByAttribute(attribute);
		int lastClass = -1;
		double lastAttrValue = 0;
		for(Instance inst:data.getInstances()) {
			if(lastClass != -1 && lastClass != inst.classValue()){
				double curThreshold = (lastAttrValue + inst.valueOf(attribute)) / 2;
				attribute.setThreshold(curThreshold);
				InstanceSet[] splitData = splitData(data, attribute);
				double curInfogain = infoGain;
				for (int i = 0; i < splitData.length; i++) {
					if (splitData[i].instanceSize() > 0) {
						curInfogain -= ((double) splitData[i].instanceSize() / 
								(double) data.instanceSize()) * computeEntropy(splitData[i]);
					}
				}
				if(curInfogain > maxInfoGain) {
					maxInfoGain = curInfogain;
					threshold = curThreshold;
				}
			}
			lastClass = inst.classValue();
			lastAttrValue = inst.valueOf(attribute);
		}
		attribute.setThreshold(threshold);
		if(DEBUG) 
			System.out.println("threshold = " + threshold + ", maxInfoGain = " + maxInfoGain);
		return maxInfoGain;
	}
	
	/**
	 * Compute the entropy of the data set
	 * @param data the instances
	 * @return entropy
	 */
	private double computeEntropy(InstanceSet data) {
		int[] classCounts = new int[data.classSize()];
		for (Instance inst:data.getInstances()) {
			classCounts[(int)inst.classValue()]++;
		}
		double entropy = 0;
		for (int j = 0; j < data.classSize(); j++) {
			if (classCounts[j] > 0) {
				entropy -= classCounts[j] * log2(classCounts[j]);
			}
		}
		entropy /= (double) data.instanceSize();
		return entropy + log2(data.instanceSize());
	}
	
	/**
	 * Split the instances into several instance set on the attribute
	 * @param data the instances to be split
	 * @param attr the attribute to be split on
	 * @return the array of split instance set 
	 */
	private InstanceSet[] splitData(InstanceSet data, Attribute attr) {
		int size = attr.isNumeric() ? 2 : attr.valueSize();
		InstanceSet[] splitData = new InstanceSet[size];
		for (int j = 0; j < size; j++) {
			splitData[j] = new InstanceSet(data);
		}
		for(Instance inst:data.getInstances()) {
			int index = (int)inst.valueOf(attr);
			if(attr.isNumeric()) {
				index = inst.valueOf(attr) <= attr.getThreshold() ? 0 : 1;
			}
			splitData[index].addInstance(inst);
		}
		return splitData;
	}
	
	/**
	 * Cross validation on the data set
	 * @param data the instances
	 * @param numFolds number of folds
	 * @return the accuracy of cross validation
	 * @throws Exception
	 */
	public double crossValidate(InstanceSet data, int numFolds) throws Exception {
		if(numFolds < 2) {
			numFolds = Math.min(5, data.instanceSize());
		}
		data = new InstanceSet(data, true);
		if (data.classAttribute().isNominal()) {
			data.stratify(numFolds);
		}
		int accurateNum = 0, totalNum = data.instanceSize();
		for (int i = 0; i < numFolds; i++) {
			InstanceSet trainSet = data.crossvalTrainSet(numFolds, i);
			InstanceSet testSet = data.crossvalTestSet(numFolds, i);
			buildTree(trainSet);
			accurateNum += evaluate(testSet);
		}
		double accuracy = accurateNum * 100.0 / totalNum;
		return accuracy;
	}
	
	/**
	 * Predict result on the test data set and print it out
	 * @param data the test data set
	 * @return the number of accurate evaluations
	 */
	private int evaluate(InstanceSet data) {
		int accurateNum = 0;
		for(Instance inst:data.getInstances()) {
			int predictedClass = classifyInstance(inst);
			String label = predictedClass >= 0 ? data.classAttribute().valueAt(predictedClass) : "?";
			System.out.println(inst.toString() + " => " + label);
			if(predictedClass == inst.classValue()) {
				accurateNum++;
			}
		}
		return accurateNum;
	}
	
	/**
	 * Use the training set to build the decision tree, then evaluate on test data set
	 * @param trainset the training data set
	 * @param testset the test data set
	 * @throws Exception
	 */
	public void evaluate(InstanceSet trainset, InstanceSet testset) throws Exception {
		buildTree(trainset);
		evaluate(testset);
	}
	
	/**
	 * Returns index of minimum element in a given array of integers.
	 * First minimum is returned.
	 *
	 * @param ints the array of integers
	 * @return the index of the minimum element
	 */
	public static int maxIndex(double[] data) {
		double maximum = 0;
		int maxIndex = 0;
		for (int i = 0; i < data.length; i++) {
			if ((i == 0) || (data[i] > maximum)) {
				maxIndex = i;
				maximum = data[i];
			}
		}
	    return maxIndex;
	}
	
	/**
	 * Normalizes the doubles in the array by their sum.
	 * @param doubles the array of double
	 * @exception Exception if sum is NaN
	 */
	public static void normalize(double[] data) throws Exception {
		double sum = 0;
		for (int i = 0; i < data.length; i++) {
			sum += data[i];
		}
		if (Double.isNaN(sum)) {
			throw new Exception("Array contains NaN - can't normalize");
		}
		if (sum != 0) {
			for (int i = 0; i < data.length; i++) {
				data[i] /= sum;
			}
		}
	}
	
	/**
	 * Returns the logarithm of a for base 2.
	 * @param a double
	 */
	public static double log2(double a) {
		return Math.log(a) / LOG2;
	}
	
	/**
	 * Tests if a is equal to b.
	 * @param a double
	 * @param b double
	 */
	public static boolean eq(double a, double b) {
		return Math.abs(a-b) < EPS;
	}
	
	public static void main(String[] args) throws Exception {
		String trainFile = "data/trainProdSelection.arff"; //"data/trainProdIntro.binary.arff";
		trainFile = args[0];
		String testFile = "data/testProdSelection.arff"; //"data/testProdIntro.binary.arff";
		testFile = args[1];
		
		ID3 dt = new ID3();
		InstanceSet trainset = new InstanceSet(trainFile);
		double accuracy = dt.crossValidate(trainset, 5);
		System.out.println("Cross-Validataion Accuracy: " + accuracy);
		
		InstanceSet testset = new InstanceSet(testFile);
		System.out.println("\nTest data predict result:");
		dt.evaluate(trainset, testset);
	}
}
