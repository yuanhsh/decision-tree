import java.util.*;

public class Attribute {

	// type for numeric/continuous attributes.
	public final static int NUMERIC = 0;

	// type for nominal/discrete attributes.
	public final static int NOMINAL = 1;
	
	private String name; // the attribute's name
	private int type = NUMERIC; // the attribute's type
	private int index = -1; // the attribute's index in the data set attribute list
	private List<String> values; // the attribute's values if it is a nominal attribute
	private double threshold; // the splitting threshold if this is a numeric attribute
	
	public Attribute(String name, int index) {
		this.name = name;
		this.index = index;
	}
	
	public Attribute(String name, List<String> values, int index) {
		this.name = name;
		this.values = values;
		this.type = NOMINAL;
		this.index = index;
	}
	
	public boolean isNominal() {
		return (type == NOMINAL);
	}

	public boolean isNumeric() {
		return (type == NUMERIC);
	}

	public String valueAt(int idx) {
		if(type != NOMINAL) return "";
		return values.get(idx);
	}
	
	public int indexOf(String value) {
		if(this.isNumeric()) return -1;
		return this.values.indexOf(value);
	}
	
	public int valueSize() {
		return values.size();
	}

	public String getName() {
		return name;
	}

	public int getType() {
		return type;
	}

	public int getIndex() {
		return index;
	}

	public List<String> getValues() {
		return values;
	}
	
	public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("@attribute ").append(this.getName() + " ");
		if(this.isNumeric()) {
			sb.append("real");
		} else {
			String vals = Arrays.toString(this.getValues().toArray()).
					replace("[", "{").replace("]", "}").replace(" ", "");
			sb.append(vals);
		}
		return sb.toString();
	}
	
}
